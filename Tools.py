import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.metrics import mean_squared_log_error

def describe_data(data, meta):
    desc = data.apply(describe_column(meta)).T
    desc = desc.style.set_properties(**{'text-align': 'left'})
    desc = desc.set_table_styles([ dict(selector='th', props=[('text-align', 'left')])])
    return desc


def describe_column(meta):
    """
    Utility function for describing a dataset column (see below for usage)
    """
    def f(x):
        d = pd.Series(name=x.name, dtype=object)
        m = next(m for m in meta if m['name'] == x.name)
        d['Type'] = m['type']
        d['#NaN'] = x.isna().sum()
        d['Description'] = m['desc']
        if m['type'] == 'categorical':
            counts = x.dropna().map(dict(enumerate(m['cats']))).value_counts().sort_index()
            d['Statistics'] = ', '.join(f'{c}({n})' for c, n in counts.items())
        elif m['type'] == 'real' or m['type'] == 'integer':
            stats = x.dropna().agg(['mean', 'std', 'min', 'max'])
            d['Statistics'] = ', '.join(f'{s}={v :.1f}' for s, v in stats.items())
        elif m['type'] == 'boolean':
            counts = x.dropna().astype(bool).value_counts().sort_index()
            d['Statistics'] = ', '.join(f'{c}({n})' for c, n in counts.items())
        else:
            d['Statistics'] = f'#unique={x.nunique()}'
        return d
    return f

def describe_data_nan(data):
    desc = data.apply(describe_column_nans()).T
    desc = desc.style.set_properties(**{'text-align': 'left'})
    desc = desc.set_table_styles([ dict(selector='th', props=[('text-align', 'left')])])
    return desc

def describe_column_nans():
    """
    Utility function for describing a dataset column (see below for usage)
    """
    def f(x):
        d = pd.Series(name=x.name, dtype=object)
        d['#NaN'] = x.isna().sum()
        return d
    return f

def root_mean_squared_log_error(y_true, y_pred):
    # Alternatively: sklearn.metrics.mean_squared_log_error(y_true, y_pred) ** 0.5
    assert (y_true >= 0).all()
    assert (y_pred >= 0).all()
    log_error = np.log1p(y_pred) - np.log1p(y_true)  # Note: log1p(x) = log(1 + x)
    return np.mean(log_error ** 2) ** 0.5

def correlation(df):
    corr = df.corr()
    mask = np.triu(np.ones_like(corr, dtype=bool))

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(11, 9))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(230, 20, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.5, center=0,
                square=True, linewidths=.5, cbar_kws={"shrink": .5})

# mean earth radius - https://en.wikipedia.org/wiki/Earth_radius#Mean_radius
_AVG_EARTH_RADIUS_KM = 6371.0088

# Unit values taken from http://www.unitconversion.org/unit_converter/length.html
_CONVERSION_TO_KILOMETERS = 1.0
_CONVERSION_TO_METERS = 1000.0
_CONVERSION_TO_MILES = 0.621371192
_CONVERSION_TO_NAUTICAL_MILES = 0.539956803
_CONVERSION_TO_FEETS = 3280.839895013
_CONVERSION_TO_INCHES = 39370.078740158

def _get_avg_earth_radius(unit):
	if unit == 'km':
		return _AVG_EARTH_RADIUS_KM
	elif unit == 'm':
		return _AVG_EARTH_RADIUS_KM * _CONVERSION_TO_METERS
	elif unit == 'mile':
		return _AVG_EARTH_RADIUS_KM * _CONVERSION_TO_MILES
	elif unit == 'nmile':
		return _AVG_EARTH_RADIUS_KM * _CONVERSION_TO_NAUTICAL_MILES
	elif unit == 'feet':
		return _AVG_EARTH_RADIUS_KM * _CONVERSION_TO_FEETS
	elif unit == 'inch':
		return _AVG_EARTH_RADIUS_KM * _CONVERSION_TO_INCHES
	else:
		raise ValueError('unit should be "km", "m", "mile", "nmile", "feet" or "inch". Found {0}.'.format(unit))

def haversine(points1, points2=(55.754093, 37.620407), unit='km'):
	avg_earth_radius = _get_avg_earth_radius(unit)
	
	points2 = np.radians(points2)

	lat1, lon1 = np.radians(points1[0]), np.radians(points1[1])
	lat2, lon2 = points2[0], points2[1]

	lat_diff = lat1 - lat2
	lon_diff = lon1 - lon2

	d = np.sin(lat_diff * 0.5) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(lon_diff * 0.5) ** 2

	return 2 * avg_earth_radius * np.arcsin(np.sqrt(d))


def haversineManh(points1, points2=(55.754093, 37.620407), unit='km'):
    avg_earth_radius = _get_avg_earth_radius(unit)

    points2 = np.radians(points2)

    lat1, lon1 = np.radians(points1[0]), np.radians(points1[1])
    lat2, lon2 = points2[0], points2[1]

    lat_diff = lat1 - lat2
    lon_diff = lon1 - lon2

    d = np.sin(lat_diff * 0.5) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(lon_diff * 0.5) ** 2

    return 2 * avg_earth_radius * np.arcsin(np.sqrt(d))

def eval(model, X_train, X_test, y_train, y_test) -> None:
    y_hat_test = model.predict(X_test)
    y_hat_test[y_hat_test < 0] = y_train.median()
    test_score = mean_squared_log_error(y_hat_test, y_test) ** 0.5
    y_hat_train = model.predict(X_train)
    y_hat_train[y_hat_train < 0] = y_train.median()
    train_score = mean_squared_log_error(y_hat_train,y_train) ** 0.5
    print(f"Train_score: {train_score}, Test_score: {test_score}")

def get_mean(df0: pd.DataFrame, df1: pd.DataFrame, column: str)-> float:
    """
    returns the mean of a column between two dataframes
    """
    return (df0[column].sum() + df1[column].sum()) / (df0[column].count() + df1[column].count())

def get_mode(df0: pd.DataFrame, df1: pd.DataFrame, column: str) -> int:
    return pd.concat((df0, df1))[column].mode()
    
def difference_train_test(df_train, df_test , column):
    one = df_train[column].unique()
    two = df_test[column].unique()
    merged = pd.concat((df_train[column],df_test[column]))
    intersect = np.intersect1d(one,two)

    print("")
    print("")
    print("Intersects ("+column+"):")
    print(len(intersect))
    print("")
    print("Total Uniques ("+column+")")
    print(len(merged.unique()))
    diff = len(merged.unique()) - len(intersect)
    return diff

def closest_node(node, nodes):
    nodes = np.asarray(nodes)
    deltas = nodes - node
    dist_2 = np.einsum('ij,ij->i', deltas, deltas)
    return np.argmin(dist_2)