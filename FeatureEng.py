import numpy as np
import pandas as pd
from pandas.io.json import json_normalize
import Tools

def create_bathroom_sum(df: pd.DataFrame) -> None:
    """
    Create column for the sum of private and shared bathrooms
    """
    df["bathroom_sum"] = df.bathrooms_private + df.bathrooms_shared

    # if the appartment have a private bathroom add 1?
    # df.loc[df.bathroom_private == 1, "bathroom_sum"] += 1


def create_bal_log_sum(df: pd.DataFrame) -> None:
    df["bal_log_sum"] = df.loggias + df.balconies


def create_seller_dummies(df: pd.DataFrame) -> pd.DataFrame:
    return pd.get_dummies(df, columns=["seller"], dummy_na=True)


def create_cond_dummies(df: pd.DataFrame) -> pd.DataFrame:
    return pd.get_dummies(df, columns=["condition"], dummy_na=True)


def create_material_dummies(df: pd.DataFrame) -> pd.DataFrame:
    return pd.get_dummies(df, columns=["material"], dummy_na=True)


def create_district_dummies(df: pd.DataFrame) -> pd.DataFrame:
    return pd.get_dummies(df, columns=["district"], dummy_na=True)


def create_parking_dummies(df: pd.DataFrame) -> pd.DataFrame:
    return pd.get_dummies(df, columns=["parking"], dummy_na=True)


def create_layout_dummies(df: pd.DataFrame) -> pd.DataFrame:
    return pd.get_dummies(df, columns=["layout"], dummy_na=True)


def create_dummy(df: pd.DataFrame, column) -> pd.DataFrame:
    return pd.get_dummies(df, columns=[column], dummy_na=True)


def create_dummies(df: pd.DataFrame, columns) -> pd.DataFrame:
    return pd.get_dummies(df, columns=columns, dummy_na=True)



def metro_dist(data):
    geojson = pd.read_json('data/response.json')
    metros = pd.json_normalize(geojson["features"])
    metros = metros.drop(columns=["type","geometry.type","properties.DatasetId"])
    metros = metros.rename(columns={"geometry.coordinates": "loc", "properties.Attributes.global_id" : "id"})
    metros = metros[['id','loc']]
    metros[["lat", "long"]] = pd.DataFrame(metros["loc"].tolist(), columns=["lat", "long"])
    metros = metros.drop(columns="loc")

    dist = None
    for idx in data.building_id:
        #print([data.loc[data.id == idx, "longitude"],data.loc[data.id == idx, "latitude"]])
        dist = Tools.haversine([metros.lat.values,metros.long.values], [data.loc[data.building_id == idx, "longitude"].unique(), data.loc[data.building_id == idx, "latitude"].unique()],unit="m")
        data.loc[data.building_id == idx, "dist_metro"] = dist.min()

    return data

def parks_dist(data):
    """
    Reads a list of park coordiantes, and calculates each appartments distance so the closest park.
    """
    parks = pd.read_xml("data/parks.kml")
    
    # drop irrelevant columns
    parks.drop(["description","Snippet","name"], axis=1, inplace=True)
    # remove extra ",0" at the end of the rows
    parks.coordinates = parks.coordinates.str[:-2]
    
    # split into lat and long
    new = parks["coordinates"].str.split(",", n=1,expand=True)
    parks["lat"] = new[1]
    parks["long"] = new[0]
    
    # cast to float
    parks.lat = parks.lat.astype(float)
    parks.long = parks.long.astype(float)

    # drop the old coordinates
    parks.drop("coordinates", axis=1, inplace=True)
    
    # calculate the distances
    dist = None
    for idx in data.building_id.unique():
        #print([data.loc[data.id == idx, "longitude"],data.loc[data.id == idx, "latitude"]])
        dist = Tools.haversine([parks.long.values, parks.lat.values], [data.loc[data.building_id == idx, "longitude"].unique(), data.loc[data.building_id == idx, "latitude"].unique()], unit="m")
        data.loc[data.building_id == idx, "dist_parks"] = dist.min()
    return data

def high_appartment(df: pd.DataFrame) -> pd.DataFrame:
    """
    Boolean if the appartment is high up or not in relation to floor and stories
    """
    height = df.floor.divide(df.stories)
    high_exp = np.exp(height) - 1
    e = np.exp(1)

    df['high'] = high_exp
    df['high'].where(df['high'] > e, e)

    return df

def middle(df:pd.DataFrame) -> pd.DataFrame:
    """
    Boolean 1 if the appartment is not floor 1 or top floor
    """
    df["middle"] = 1

    mask = (df.floor == df.stories) | (df.floor == 1)
    # set to 0
    df.loc[mask,"middle"] = 0
    return df

def sq_diff(data):
    #data["sq_diff"] = data.area_total - data.area_kitchen
    data["room_size"] = data.area_living/data.rooms
    return data


def floor_ratio(data):
    for idx in data.building_id:
        data.loc[data.building_id == idx, "floor_ratio"] = data.loc[data.building_id == idx, "stories"]
    return data


def district_avg_price(data, data_test):
    for idx in data.district.unique():
        data.loc[data.district == idx, "avg_price"] = np.mean(data.price)/np.mean(data.total_area)
        data_test.loc[data_test.district == idx, "avg_price"] = np.mean(data.price)/np.mean(data.area)
    return data, data_test


def university_dist(data):
    universities = [
        (55.67249540277297, 37.57367075739323),
        (55.848593408183014, 37.63366154242958),
        (55.6927502611028, 37.39749771164194),
        (55.85989907696161, 37.47471830960826),
        (55.71275216609847, 37.39960668456914),
        (55.668081662254856, 37.50617732696646),
        (55.73388744190375, 37.43235888837524),
        (55.67545847268359, 37.57650405765294),
        (55.76998506895896, 37.71307631112898),
        (55.636381586267106, 37.61111962636034),
        (55.78362014503218, 37.660868227044766),
        (55.69875568681356, 37.39433724232817),
        (55.82769889412336, 37.46133846385291),
        (55.8716119484216, 37.31466813820559),
        (55.77763401914524, 37.43414293198491)
    ]
    uni = pd.DataFrame(universities)

    dist = None
    for idx in data.building_id.unique():
        origin = list(zip(data.loc[data.building_id == idx, "longitude"].unique(), data.loc[data.building_id == idx, "latitude"].unique()))
        destinations = list(zip(uni[1], uni[0]))
        dist = Tools.haversine([uni[1].values, uni[0].values], [data.loc[data.building_id == idx, "longitude"].unique(), data.loc[data.building_id == idx, "latitude"].unique()], unit="m")
        data.loc[data.building_id == idx, "dist_uni"] = dist.min()
        data.loc[data.building_id == idx, "uni"] = Tools.closest_node(origin, destinations)

    return data


def hub_dist(data):
    geojson = pd.read_json('data/transporthubs.json')
    hubs = pd.json_normalize(geojson["features"])
    hubs = hubs[["geometry.coordinates", "properties.Attributes.ID_en"]]
    hubs = hubs.rename(columns={"geometry.coordinates": "loc", "properties.Attributes.ID_en": "id"})
    hubs[["lat", "long"]] = pd.DataFrame(hubs["loc"].tolist(), columns=["lat", "long"])
    hubs = hubs.drop(columns="loc")

    dist = None
    for idx in data.building_id.unique():
        # print([data.loc[data.id == idx, "longitude"],data.loc[data.id == idx, "latitude"]])
        dist = Tools.haversine([hubs.lat.values, hubs.long.values],
                               [data.loc[data.building_id == idx, "longitude"].unique(), data.loc[data.building_id == idx, "latitude"].unique()], unit="m")
        data.loc[data.building_id == idx, "dist_hub"] = dist.min()

    return data


def train_station_dist(data):
    geojson = pd.read_json('data/trainstations.json')
    train_stations = pd.json_normalize(geojson["Cells"])
    train_stations = train_stations[["geoData.coordinates", "global_id"]]
    train_stations = train_stations.rename(columns={"geoData.coordinates": "loc", "global_id": "id"})
    train_stations[["lat", "long"]] = pd.DataFrame(train_stations["loc"].tolist(), columns=["lat", "long"])
    train_stations = train_stations.drop(columns="loc")

    dist = None
    for idx in data.building_id.unique():
        # print([data.loc[data.id == idx, "longitude"],data.loc[data.id == idx, "latitude"]])
        dist = Tools.haversine([train_stations.lat.values, train_stations.long.values],
                               [data.loc[data.building_id == idx, "longitude"].unique(), data.loc[data.building_id == idx, "latitude"].unique()], unit="m")
        data.loc[data.building_id == idx, "dist_train_station"] = dist.min()

    return data


def gym_dist(data):
    geojson = pd.read_json('data/gyms.json')
    gyms = pd.json_normalize(geojson["features"])
    gyms = gyms[["geometry.coordinates", "properties.Attributes.global_id"]]
    gyms = gyms.rename(columns={"geometry.coordinates": "loc", "properties.Attributes.global_id": "id"})
    gyms[["lat", "long"]] = pd.DataFrame(gyms["loc"].tolist(), columns=["lat", "long"])
    gyms = gyms.drop(columns="loc")

    dist = None
    for idx in data.building_id.unique():
        # print([data.loc[data.id == idx, "longitude"],data.loc[data.id == idx, "latitude"]])
        dist = Tools.haversine([gyms.lat.values, gyms.long.values],
                               [data.loc[data.building_id == idx, "longitude"].unique(), data.loc[data.building_id == idx, "latitude"].unique()], unit="m")
        data.loc[data.building_id == idx, "dist_gym"] = dist.min()

    return data


def stadium_dist(data):
    stadiums = [
        (55.791389, 37.559722),
        (55.803611, 37.741111),
        (55.791389, 37.516111),
        (55.817861, 37.44025),
        (55.715556, 37.553611),
    ]
    uni = pd.DataFrame(stadiums)

    dist = None
    for idx in data.id:
        dist = Tools.haversine([uni[1].values,uni[0].values], [data.loc[data.id == idx, "longitude"], data.loc[data.id == idx, "latitude"]],unit="km")
        data.loc[data.id == idx, "dist_stadiums"] = dist.min()

    return data

def water_facilites(data):
    water_facilites = [
        (55.661634674466995, 37.700948709661304),
        (55.686441683977975, 37.94336506164841),
    ]
    uni = pd.DataFrame(water_facilites)

    dist = None
    for idx in data.id:
        dist = Tools.haversine([uni[1].values,uni[0].values], [data.loc[data.id == idx, "longitude"], data.loc[data.id == idx, "latitude"]],unit="km")
        data.loc[data.id == idx, "dist_water_treatment"] = dist.min()

    return data


def closest_transport_stop(data):
    geojson = pd.read_json('data/surfacetransportstops.json')
    stops = pd.json_normalize(geojson["Cells"])
    stops = stops[["Latitude_WGS84_en", "Longitude_WGS84_en", "global_id"]]
    stops = stops.rename(columns={"Latitude_WGS84_en": "lat", "Longitude_WGS84_en": "long", "global_id": "id"})
    stops.lat = stops.lat.astype(float, errors="raise")
    stops.long = stops.long.astype(float, errors="raise")

    dist = None
    for idx in data.building_id.unique():
        # print([data.loc[data.id == idx, "longitude"],data.loc[data.id == idx, "latitude"]])
        dist = Tools.haversine([stops.long.values, stops.lat.values],
                               [data.loc[data.building_id == idx, "longitude"].unique(),
                                data.loc[data.building_id == idx, "latitude"].unique()], unit="m")
        data.loc[data.building_id == idx, "dist_stop"] = dist.min()

    return data

