import numpy as np
import pandas as pd
import Tools
import json
import LightGBM as lg


class DataCleaning:
    """
    Outlier detection with removal
    """


def drop_outliers(df, field_name):
    upper = 80  # Percentile
    lower = 10  # Percentile
    iqr = 1.5 * (np.percentile(df[field_name], upper) - np.percentile(df[field_name], lower))
    df.drop(df[df[field_name] > (iqr + np.percentile(df[field_name], upper))].index, inplace=True)
    df.drop(df[df[field_name] < (np.percentile(df[field_name], lower) - iqr)].index, inplace=True)



def distance_binning(df):
    conditions = [
        (df.dist_from_center <= 5),
        (df.dist_from_center <= 15),
        (df.dist_from_center > 15)
    ]

    values = ['close', 'medium', 'far']
    df["close_med_far"] = np.select(conditions, values)

    df = pd.get_dummies(df, columns=["close_med_far"], dummy_na=True, prefix="dist")
    df = df.drop(columns=["dist_from_center"])
    df = df.drop(columns="dist_nan")
    return df

# Fills in building related information within each building. Sets average for ceiling and average nearest integer
# for the rest
def fillna_building_average(train,test):
    j = 0
    for i in np.unique(train.building_id):
        j += 1
        if not j % 500:
            print(j)
        train.loc[train.building_id == i, "windows_court"] = train.loc[train.building_id == i, "windows_court"].fillna(
            np.rint(Tools.get_mean(train,test,"windows_court")))
        train.loc[train.building_id == i, "windows_street"] = train.loc[train.building_id == i, "windows_street"].fillna(
            np.rint(Tools.get_mean(train,test,"windows_street")))
        train.loc[train.building_id == i, "bathrooms_shared"] = train.loc[
            train.building_id == i, "bathrooms_shared"].fillna(
            np.rint(Tools.get_mean(train,test,"bathrooms_shared")))
        train.loc[train.building_id == i, "bathrooms_private"] = train.loc[
            train.building_id == i, "bathrooms_private"].fillna(
            np.rint(Tools.get_mean(train,test,"bathrooms_private")))
        train.loc[train.building_id == i, "ceiling"] = train.loc[train.building_id == i, "ceiling"].fillna(
            Tools.get_mean(train,test,"ceiling"))
        train.loc[train.building_id == i, "balconies"] = train.loc[train.building_id == i, "balconies"].fillna(
            np.rint(Tools.get_mean(train,test,"balconies")))
        train.loc[train.building_id == i, "loggias"] = train.loc[train.building_id == i, "loggias"].fillna(
            np.rint(Tools.get_mean(train,test,"loggias")))
        train.loc[train.building_id == i, "layout"] = train.loc[train.building_id == i, "layout"].fillna(
            np.rint(Tools.get_mean(train,test,"layout")))

        train.loc[train.building_id == i, "area_kitchen"] = train.loc[train.building_id == i, "area_kitchen"].fillna(
            Tools.get_mean(train, test, "area_kitchen"))
        train.loc[train.building_id == i, "area_living"] = train.loc[train.building_id == i, "area_living"].fillna(
            Tools.get_mean(train, test, "area_living"))

def fillna_district_average(train,test):
    j = 0
    for i in np.unique(train.district):
        j += 1
        if not j % 500:
            print(j)
        train.loc[train.district == i, "windows_court"] = train.loc[train.district == i, "windows_court"].fillna(
            np.rint(Tools.get_mean(train,test,"windows_court")))
        train.loc[train.district == i, "windows_street"] = train.loc[train.district == i, "windows_street"].fillna(
            np.rint(Tools.get_mean(train,test,"windows_street")))
        train.loc[train.district == i, "bathrooms_shared"] = train.loc[train.district == i, "bathrooms_shared"].fillna(
            np.rint(Tools.get_mean(train,test,"bathrooms_shared")))
        train.loc[train.district == i, "bathrooms_private"] = train.loc[train.district == i, "bathrooms_private"].fillna(
            np.rint(Tools.get_mean(train,test,"bathrooms_private")))
        train.loc[train.district == i, "ceiling"] = train.loc[train.district == i, "ceiling"].fillna(
            Tools.get_mean(train,test,"ceiling"))
        train.loc[train.district == i, "balconies"] = train.loc[train.district == i, "balconies"].fillna(
            np.rint(Tools.get_mean(train,test,"balconies")))
        train.loc[train.district == i, "loggias"] = train.loc[train.district == i, "loggias"].fillna(
            np.rint(Tools.get_mean(train,test,"loggias")))
        train.loc[train.district == i, "layout"] = train.loc[train.district == i, "layout"].fillna(
            np.rint(Tools.get_mean(train,test,"layout")))

        train.loc[train.district == i, "seller"] = train.loc[train.district == i, "seller"].fillna(
            np.rint(Tools.get_mean(train,test,"seller")))
        train.loc[train.district == i, "area_kitchen"] = train.loc[train.district == i, "area_kitchen"].fillna(
            Tools.get_mean(train,test,"area_kitchen"))
        train.loc[train.district == i, "area_living"] = train.loc[train.district == i, "area_living"].fillna(
            Tools.get_mean(train,test,"area_living"))
        train.loc[train.district == i, "condition"] = train.loc[train.district == i, "condition"].fillna(
            np.rint(Tools.get_mean(train,test,"condition")))
        train.loc[train.district == i, "constructed"] = train.loc[train.district == i, "constructed"].fillna(
            np.rint(Tools.get_mean(train,test,"constructed")))
        train.loc[train.district == i, "material"] = train.loc[train.district == i, "material"].fillna(
            np.rint(Tools.get_mean(train,test,"material")))
        train.loc[train.district == i, "garbage_chute"] = train.loc[train.district == i, "garbage_chute"].fillna(
            np.rint(Tools.get_mean(train,test,"garbage_chute")))
        train.loc[train.district == i, "heating"] = train.loc[train.district == i, "heating"].fillna(
            np.rint(Tools.get_mean(train,test,"heating")))


        # Unsure:

        train.loc[train.district == i, "parking"] = train.loc[train.district == i, "parking"].fillna(
            np.rint(Tools.get_mean(train,test,"parking")))


def clean_parking(df):
    df.parking.fillna(3, inplace=True)
    return df


def clean_garbage_chute(df):
    df.garbage_chute.fillna(1, inplace=True)
    return df

def clean_distance_to_center(df):  # Remove outliers in distance
    df.loc[df.dist_from_center > 75, "dist_from_center"] = np.mean(df["dist_from_center"])


def drop_layout(df):
    return df.drop(columns=["layout"])


def drop_low_importance(df):
    df = df.drop(columns=["street", "address"])
    df = df.copy()
    Y = df["price"]
    X = df.drop(columns="price")

    zero_features, feature_importances = lg.identify_zero_importance_features(X, Y, iterations=2)
    print(feature_importances)

    feature_importances.describe()
    pp = np.percentile(feature_importances['importance'], 20)

    to_drop = feature_importances[feature_importances['importance'] <= pp]['feature']

    return to_drop


def drop_high_correlation(df):
    threshold = 0.78

    # Absolute value correlation matrix
    corr_matrix = df.corr().abs()

    # Upper triangle of correlations
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(bool))

    # Select columns with correlations above threshold
    to_drop = [column for column in upper.columns if any(upper[column] > threshold)]
    print(to_drop)

    return to_drop

def dummys_generator(df):
    return pd.get_dummies(df, columns=["condition"], dummy_na=True, prefix="Cond")


def fill_bathrooms_mean(df):
    df.loc[df.bathrooms_shared.isna(),["bathrooms_shared"]] = 1
    df.loc[df.bathrooms_private.isna(),["bathrooms_private"]] = 1


def clean_feature_new(df) -> None:
    """
    Some values of new are set to 1, when the construction date is more than 10 years old. And some are not set at all.
    This function fixes these values to correspond to the correct construction year.

    """
    # if new == 1 (True), but the construction year is old, set new == 0
    # update the values that dont have new but have constructed
    df.loc[(df.new.isna()) & (df.constructed < 2010), "new"] = 0
    df.loc[((df.new.isna()) & (df.constructed >= 2010)), "new"] = 1

    # update the values that have new == 1 but are from before 2010 to 0 and vice versa
    df.loc[(df.constructed >= 2010), "new"] = 1
    df.loc[(df.constructed < 2010), "new"] = 0


def set_means_constructed(df, new_mean, construced_mean):
    # for new buildings
    df.loc[((df.constructed.isna()) & (df.new == 1)), "constructed"] = new_mean
    # for old buildings
    df.loc[((df.constructed.isna()) & (df.new == 0)), "constructed"] = construced_mean

    # when new is unknown
    # set it to the mean
    df.loc[(df.constructed.isna()), "constructed"] = construced_mean
    # clustering???


def clean_feature_constructed(df_train: pd.DataFrame, df_test: pd.DataFrame) -> pd.DataFrame:
    """
    Set date for missing nan values to the mean based on if the appartment is new or not.
    """

    after_2009_mask_train = (df_train.constructed > 2009)
    after_2009_mask_test = (df_test.constructed > 2009)

    # find mean of train and test
    construced_mean = Tools.get_mean(df_train,df_test,"constructed")
    new_mean = Tools.get_mean(df_train[after_2009_mask_train],df_test[after_2009_mask_test],"constructed")

    ## When new is known
    set_means_constructed(df_train, construced_mean, new_mean)
    set_means_constructed(df_test, construced_mean, new_mean)

        # create very_new, new, old, very_old
    #df_train = create_column_age(df_test, df_train)
    #df_test = create_column_age(df_train, df_test)

    return (df_train, df_test)

def clean_feature_constructed_single(df_train: pd.DataFrame, df_test: pd.DataFrame) -> pd.DataFrame:
    """
    Set date for missing nan values to the mean based on if the appartment is new or not.
    """

    after_2009_mask_train = (df_train.constructed > 2009)
    after_2009_mask_test = (df_test.constructed > 2009)

    # find mean of train and test
    construced_mean = (df_train.constructed.sum() + df_test.constructed.sum()) / (
                df_train.constructed.count() + df_test.constructed.count())
    new_mean = (df_train[after_2009_mask_train].constructed.sum() + df_test[after_2009_mask_test].constructed.sum()) / (
            df_train[after_2009_mask_train].constructed.count() + df_test[after_2009_mask_test].constructed.count())

    ## When new is known
    set_means_constructed(df_train, construced_mean, new_mean)

        # create very_new, new, old, very_old
    #df_train = create_column_age(df_test, df_train)

    return (df_train)

def create_column_age(df_test, df_train):
    """
    make constructed into age
    then age into bins
    then bins into dummies,
    """
    df_train["age"] = ((df_train.constructed - max([df_train.constructed.max(), df_test.constructed.max()])) * -1)
    df_train["age"] = pd.cut(df_train.age, [-1, 10, 20, 40, 500], labels=["very_new", "new", "old", "very_old"])
    df_train = pd.get_dummies(df_train, columns=["age"], dummy_na=True)
    return df_train


def clean_feature_ceiling(df_train: pd.DataFrame, df_test: pd.DataFrame) -> None:
    """
    Clean feature ceiling
    # some had ceiling at > 200, meaning that they tought ceiling was in cm, and not meter.
    Others that where nan, above 10 meter ceiling height or below 1 meter are set to the mean.
    """
    df_train.loc[df_train.ceiling > 50, "ceiling"] /= 100
    df_test.loc[df_test.ceiling > 50, "ceiling"] /= 100

    df_train.loc[df_train.ceiling > 25, "ceiling"] /= 10
    df_test.loc[df_test.ceiling > 25, "ceiling"] /= 10

    df_train.loc[df_train.ceiling < 0.5, "ceiling"] = float('NaN')
    df_test.loc[df_test.ceiling < 0.5, "ceiling"] = float('NaN')

    # see that we can fill ceilings that are low, and use material 3 and 4 can be set to a sample of material 3 and 4
    df_train = clean_ceiling_by_material(df_train)
    df_test = clean_ceiling_by_material(df_test)

    # see that we can fill ceilings that are low, and use that condition 1 can be set to a sample of condtion 1
    df_train = clean_ceiling_by_condition(df_train)
    df_test = clean_ceiling_by_condition(df_test)

    ceiling_mean = (df_train.ceiling.sum() + df_test.ceiling.sum()) / (
                df_train.ceiling.count() + df_test.ceiling.count())

    df_train.loc[df_train.ceiling.isna(),"ceiling"] = ceiling_mean
    df_test.loc[df_test.ceiling.isna(),"ceiling"] = ceiling_mean


def clean_ceiling_by_material(df):
    # see that we can fill ceilings that are low, and use material 3 and 4 can be set to a sample of material 3 and 4
    mask = ((df.material == 4)| (df.material == 3)) & df.ceiling.isna()
    # 2.67 is the mean of material 3 and 4
    # samle just the low ceiling appartments
    df.loc[mask,"ceiling"] = df[df.ceiling < 2.67].ceiling.sample(n=len(df[mask]),replace=True).to_numpy()
    return df

def clean_ceiling_by_condition(df):
    # see that we can fill ceilings that are low, and use that condition 1 can be set to a sample of condtion 1
    mask = ((df.condition == 1) & (df.ceiling.isna()))
    # 2.67 is the mean of material 3 and 4
    # samle just the low ceiling appartments
    df.loc[mask,"ceiling"] = df[df.ceiling < 2.67].ceiling.sample(n=len(df[mask]),replace=True).to_numpy()
    return df

def clean_feature_heating(df: pd.DataFrame) -> pd.DataFrame:
    """
    The heating varable has a lot of missing values. The ones that are missing have a distribution close to the one of heating type 0, 1 and 3.
    Therefore we box groups 0, 1 and 3 together and assign all nans to that same group.
    """
    df.loc[(df.heating == 0) | (df.heating == 3), "heating"] = 1

    df = pd.get_dummies(df, columns=['heating'], dummy_na=True)
    return df


def clean_areas(df_train: pd.DataFrame, df_test: pd.DataFrame):
    """
    Some area_living = 0, so set it to the mean percentage of living/total.
    kitchen that is na probably has a kitchen of atleast 1 squaremeter.
    """
    # merge datasets
    prices = df_train.price
    df = df_train.drop("price",axis=1).append(df_test).copy()

    mask = (df.area_living.isna() | (df.area_living == 0))
    df.loc[mask, "area_living"] = df.loc[mask, "area_total"].values * 0.581517

    mask = (df.area_kitchen.isna() | (df.area_kitchen == 0))
    df.loc[mask, "area_kitchen"] = df.loc[mask, "area_living"].values * 0.357147

    mask = (df.area_living > df.area_total)
    df.loc[mask,["area_living"]] = df.loc[mask,["area_total"]].values

    # if kitchen is bigger than living, we found that the kitchen_area was most likely not calculated as part of the living area.
    mask = (df.area_kitchen > df.area_living)
    df.loc[mask, "area_living"] += df.loc[mask, "area_kitchen"]

    # obvious mistake
    df.loc[df.id== 1107,"area_total"] *= 10

    # clean 103 rows
    mask = (df.area_living > df.area_total) & (df.area_living == 80)
    df.loc[mask,"area_living"] *= (df[mask].area_total * 0.01)

    # flip the rest
    mask = (df.area_living > df.area_total)
    df.loc[mask, ["area_living","area_total"]] = df.loc[mask, ["area_total","area_living"]].values


    areas = [10, 20, 25, 30, 35, 40, 50]
    for area in areas:
        mask = (df.area_kitchen == area)
        df.loc[mask, "area_kitchen"] *= df.loc[mask, "area_living"] * 0.01


    # can try this instead of just areas sometime:)
    # df["area_kitchen"] *= df["area_living"] * 0.01

    # split back
    df_train = df.iloc[:len(df_train)].copy()
    df_train["price"] = prices.values
    df_test =  df.iloc[len(df_train):].copy()

    return df_train, df_test


def clean_material(df: pd.DataFrame) -> None:
    # todo: clean material
    pass


def clean_bathrooms(df_train: pd.DataFrame, df_test: pd.DataFrame) -> None:
    """
    Set bathrooms to 1

    """
    fill_bathrooms_mean(df_train)
    fill_bathrooms_mean(df_test)


def clean_balconies_loggias(df: pd.DataFrame) -> None:
    df.loc[df.balconies.isna(), ["balconies","loggias"]] = 0



# Finds closest district using coordinates
def find_closest_district(df):

    # All ids with empty district
    var = np.asarray(df.loc[df.district.isna(), "id"])

    # All rows with empty district
    rows_no_dist = df[df.id.isin(var)]

    # List with all longitude and latitude values in dataframe
    data_longlat = list(zip(df.latitude, df.longitude))

    known_streets = ["улица Центральная", "Бунинские Луга ЖК", "улица 1-я Линия"]
    # Remove  own coordinates
    for i in range(len(var)):
        n = rows_no_dist.loc[rows_no_dist["id"] == var[i]].index[0]
        data_longlat[n] = (float("nan"), float("nan"))


    copy =  data_longlat.copy()
    for j in range(len(var)):
        # Temp variable for coordinate with empty district
        if df.loc[df["id"] == var[j]].street.values[0] == known_streets[0]:
            df.at[df.loc[df["id"] == var[j]].index[0], "latitude"] = 55.610910522687554
            df.at[df.loc[df["id"] == var[j]].index[0], "longitude"] = 37.28814356811611
            df.at[df.loc[df["id"] == var[j]].index[0], "district"] = 11.0
            continue

        if df.loc[df["id"] == var[j]].street.values[0] == known_streets[1]:
            df.at[df.loc[df["id"] == var[j]].index[0], "latitude"] = 55.54382960763567
            df.at[df.loc[df["id"] == var[j]].index[0], "longitude"] = 37.48237108490257
            df.at[df.loc[df["id"] == var[j]].index[0], "district"] = 11.0
            continue

        if df.loc[df["id"] == var[j]].street.values[0] == known_streets[2]:
            df.at[df.loc[df["id"] == var[j]].index[0], "latitude"] = 55.6297901894583
            df.at[df.loc[df["id"] == var[j]].index[0], "longitude"] = 37.1461596771594
            df.at[df.loc[df["id"] == var[j]].index[0], "district"] = 11.0
            continue

        a = (float(df.loc[df["id"] == var[j]].latitude), float(df[df["id"] == var[j]].longitude))

                
        if a == (55.59516, 37.741109):
            df.at[df.loc[df["id"] == var[j]].index[0], "district"] = 11.0
            continue

        print("", a)#, df.loc[df["id"] == var[j]].street, df.loc[df["id"] == var[j]].address)
        if df.loc[df["id"] == var[j]].street.values[0] not in known_streets and not np.isnan(a[0]):


            # Select the row with nearest longitude and latitude
            nearest = min(copy, key=lambda x: distance(x, a))

            while np.isnan(df.iloc[[copy.index(nearest)]].district.values[0]):
                copy.remove(nearest)
                nearest = min(copy, key=lambda x: distance(x, a))

            # Set closest district
            df.at[df.loc[df["id"] == var[j]].index[0], "district"] = df.iloc[[copy.index(nearest)]].district.values[0]
            copy =  data_longlat.copy()


# Calculates distance using pythagoras
def distance(co1, co2):
    return np.sqrt(pow(abs(co1[0] - co2[0]), 2) + pow(abs(co1[1] - co2[1]), 2))

def feature_clean_average(df):
    # Generate column "distance from centrum"
    newcords=Tools.haversine([df.latitude.values, df.longitude.values],unit="km")
    df['dist_from_cent'] = newcords
    df = distance_binning(df)

    # Drop layout
    df = df.drop(columns=["layout"])
    df["elevator_passenger"].value_counts()

    # Fill parking and garbage
    df['parking']=df['parking'].fillna(df['parking'].mode()[0])
    clean_garbage_chute(df)

    # Fill area
    df['area_kitchen']=df['area_kitchen'].fillna(df['area_kitchen'].mean())
    df['area_living']=df['area_living'].fillna(df['area_living'].mean())

    # Fill heating, material,seller and windows
    df['heating']=df['heating'].fillna(df['heating'].mode()[0])
    df['material']=df['material'].fillna(df['material'].mode()[0])
    df['seller']=df['seller'].fillna(df['seller'].mode()[0])
    df['windows_court']=df['windows_court'].fillna(df['windows_court'].mode()[0])
    df['windows_street']=df['windows_street'].fillna(df['windows_street'].mode()[0])

    # If data is missing any districts use closest coordinates to find district
    #if df['district'].isnull().values.any():
    #    find_closest_district(df)
    # Find district with coords

    # Fill elevator data
    df["elevator_service"]=df['elevator_service'].fillna(df['elevator_service'].mode()[0])
    df["elevator_without"]=df['elevator_without'].fillna(df['elevator_without'].mode()[0])
    df["elevator_passenger"]=df['elevator_passenger'].fillna(df['elevator_passenger'].mode()[0])


def feature_clean_average_both(df_train, df_test):
    # merge datasets
    prices = df_train.price
    df = df_train.drop("price",axis=1).append(df_test).copy()

    ### MODE ###
    columns= ["parking", "heating", "material", "seller", "windows_court", "windows_street", "elevator_without", "elevator_service", "elevator_passenger", "garbage_chute", "loggias", "balconies", "bathrooms_shared", "bathrooms_private", "layout"]

    for column in columns:
        df.loc[df[column].isna(),column] = df[column].mode()[0]

    # If data is missing any districts use closest coordinates to find district
    #if df['district'].isnull().values.any():
        #find_closest_district(df)

    # split back
    df_train = df.iloc[:len(df_train)].copy()
    df_train["price"] = prices.values
    df_test =  df.iloc[len(df_train):].copy()

    return df_train, df_test
