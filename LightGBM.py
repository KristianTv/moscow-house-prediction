import lightgbm as lgb
import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd

def identify_zero_importance_features(X, y, iterations=2):
    """
    Identify zero importance features in a training dataset based on the
    feature importances from a gradient boosting model.

    Parameters
    --------
    train : dataframe
        Training features

    train_labels : np.array
        Labels for training data

    iterations : integer, default = 2
        Number of cross validation splits to use for determining feature importances
    """

    # Initialize an empty array to hold feature importances
    feature_importances = np.zeros(X.shape[1])

    # Create the model with several hyperparameters
    model = lgb.LGBMRegressor(objective='regression', boosting_type='goss',
                              n_estimators=550, class_weight='balanced')

    # Fit the model multiple times to avoid overfitting
    for i in range(iterations):
        # Split into training and validation set
        train_features, valid_features, train_y, valid_y = train_test_split(X, y,
                                                                            test_size=0.25,
                                                                            random_state=i)

        # Train using early stopping
        model.fit(train_features, train_y, early_stopping_rounds=100,
                  eval_set=[(valid_features, valid_y)])

        # Record the feature importances
        feature_importances += model.feature_importances_ / iterations

    feature_importances = pd.DataFrame({'feature': list(X.columns),
                                        'importance': feature_importances}).sort_values('importance',
                                                                                        ascending=False)

    # Find the features with zero importance
    zero_features = list(feature_importances[feature_importances['importance'] == 0.0]['feature'])
    print('\nThere are %d features with 0.0 importance' % len(zero_features))

    return zero_features, feature_importances