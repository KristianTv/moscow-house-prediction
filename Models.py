from xgboost.sklearn import XGBRegressor
from lightgbm import LGBMRegressor
from catboost import CatBoostRegressor
from sklearn.ensemble import RandomForestRegressor

from sklearn.pipeline import Pipeline
from sklearn.metrics import mean_squared_log_error
from sklearn.model_selection import GroupKFold, GridSearchCV, KFold
from sklearn.feature_selection import SelectKBest, chi2

import numpy as np


class BasicModel:
    def __init__(self, model, pipeline, params):
        self.model = model
        self.pipeline = pipeline
        self.params = params

    def grid_search_group(self, train, groups):
        print("Preforming GridSearch")
        clf = GridSearchCV(estimator= self.pipeline, param_grid=self.params,
                            scoring = 'neg_mean_squared_log_error',n_jobs=-1,verbose = 1,refit=True,
                            return_train_score = True,cv=GroupKFold(n_splits=10))
        # Perform the grid search
        clf.fit(train.drop(["price"],axis=1), train.price,groups=groups)

        #print("BEST ESTIMATOR: \n",clf.best_estimator_)
        print("Best score was: ", round(clf.best_score_,3))
        print("Best params: ", clf.best_params_)
        print("Best models std: ",clf.cv_results_['std_test_score'][clf.best_index_])

        self.model = clf.best_estimator_
        #self.pipeline["clf"] = self.model
        self.clf = clf

    def grid_search(self, train):
        print("Preforming GridSearch")
        clf = GridSearchCV(estimator= self.pipeline, param_grid=self.params,
                            scoring = 'neg_mean_squared_log_error',n_jobs=-1,verbose = 1,refit=True,
                            return_train_score = True,cv=KFold(n_splits=10))
        # Perform the grid search
        clf.fit(train.drop(["price"],axis=1), train.price)

        #print("BEST ESTIMATOR: \n",clf.best_estimator_)
        print("Best score was: ", round(clf.best_score_,3))
        print("Best params: ", clf.best_params_)
        print("Best models std: ",clf.cv_results_['std_test_score'][clf.best_index_])

        self.model = clf.best_estimator_
        #self.pipeline["clf"] = self.model
        self.clf = clf
    

    def get_model(self):
        return self.pipeline["clf"]

    def evaluate(self, X_train, X_test, y_train, y_test, log2=False) -> None:
        
        y_hat_test = self.model.predict(X_test)
        y_hat_test[y_hat_test < 0] = y_train.median()
        if log2:
            y_hat_test = 2 ** y_hat_test
        test_score = self.root_mean_squared_log_error(y_hat_test, y_test)
        
        y_hat_train = self.model.predict(X_train)
        y_hat_train[y_hat_train < 0] = y_train.median()
        if log2:
            y_hat_train = 2 ** y_hat_train
        train_score = self.root_mean_squared_log_error(y_hat_train,y_train)
        print(f"Train_score: {train_score}, Test_score: {test_score}")

    def root_mean_squared_log_error(self, y_true, y_pred):
        assert (y_true >= 0).all()
        assert (y_pred >= 0).all()
        log_error = np.log1p(y_pred) - np.log1p(y_true)  # Note: log1p(x) = log(1 + x)
        return np.mean(log_error ** 2) ** 0.5

    def predict(self, data):
        return self.model.predict(data)

    def refit(self, train):
        clf = GridSearchCV(estimator= self.pipeline, param_grid=self.params,
                            scoring = 'neg_mean_squared_log_error',n_jobs=-1,verbose = 1,refit=True,
                            return_train_score = True,cv=KFold(n_splits=10))
        # Perform the grid search
        clf.fit(train.drop(["price"],axis=1), train.price)

        #print("BEST ESTIMATOR: \n",clf.best_estimator_)
        print("Best score was: ", round(clf.best_score_,3))
        print("Best params: ", clf.best_params_)
        print("Best models std: ",clf.cv_results_['std_test_score'][clf.best_index_])

        self.model = clf.best_estimator_
        self.clf = clf


class XgbModel(BasicModel):
    def __init__(self):
        model = XGBRegressor(random_state=47, verbosity=1, tree_method = "hist", n_estimators=50)
        pipeline = Pipeline(
        [   
            ('clf', model)
        ]
        )

        params={
            'clf__scale_pos_weight':[0],
            'clf__n_estimators': [1000],
            'clf__learning_rate': [0.05],
            'clf__max_depth':[9, 10],
            'clf__random_seed':[1],
            'clf__sampling_method':["uniform"],
        }
        super().__init__(model, pipeline, params)


class XgbSmol(BasicModel):
    def __init__(self):
        model = XGBRegressor(random_state=47, verbosity=1, tree_method = "hist", n_estimators=50)
        pipeline = Pipeline(
        [   
            ('clf', model)
        ]
        )

        params={
            # 'clf__scale_pos_weight':[0],
            'clf__n_estimators': [100, 200],
            'clf__learning_rate': [0.1],
            'clf__max_depth':[5, 6, 7, 8, 9, 10],
            'clf__random_seed':[1],
            'clf__sampling_method':["uniform"],
        }
        super().__init__(model, pipeline, params)


class XGBHeavy(BasicModel):
    def __init__(self):
        model = XGBRegressor(random_state=47, verbosity=1, tree_method = "hist", n_estimators=50)
        pipeline = Pipeline(
        [   
            ('clf', model)
        ]
        )

        params={
            'clf__n_estimators': [5000],
            'clf__learning_rate': [0.01],
            'clf__subsample': [0.6]
        }
        super().__init__(model, pipeline, params)


class LGBM(BasicModel):
    def __init__(self):
        model = LGBMRegressor(random_state=47)
        pipeline = Pipeline(
        [   
            ('clf', model),
        ]
        )

        # parameters to check
        params={
            'clf__n_estimators': [1000],
            'clf__max_depth': range(10, 15),
            'clf__num_leaves':[50, 75, 120, 150],
            'clf__min_data_in_leaf':[50, 75, 100, 1000],
        }
        super().__init__(model, pipeline, params)


class CatBoost(BasicModel):
    def __init__(self):
        model = CatBoostRegressor(random_seed=47)
        pipeline = Pipeline(
        [   
            ('clf', model)
        ]
        )

        # parameters to check
        params={
            # TODO: find parameters to optimize
            'clf__depth': [8, 9],#range(6, 10, 1),
            'clf__iterations':[2000],
            'clf__learning_rate':[0.05]
        }
        super().__init__(model, pipeline, params)

class CatBoostBig(BasicModel):
    def __init__(self):
        model = CatBoostRegressor()
        pipeline = Pipeline(
        [   
            ('clf', model)
        ]
        )

        # parameters to check
        params={
            # TODO: find parameters to optimize
            'clf__depth': [8, 9, 10],
            'clf__iterations':[5000],
            'clf__learning_rate':[0.2, 0.05, 0.01]
        }
        super().__init__(model, pipeline, params)


class RF(BasicModel):
    def __init__(self):
        model = RandomForestRegressor(
            random_state=10,
            criterion='squared_error',
        )

        pipeline = Pipeline(
            [
                ('clf', model)
            ]
        )

        # parameters to check
        forest_params={
            'clf__n_estimators':[180, 500, 1000],
            'clf__max_depth':[10, 40],
            'clf__max_features':['auto'],
            'clf__min_samples_leaf':[3],
            'clf__min_samples_split':[3]
        }
        
        super().__init__(model, pipeline, forest_params)
